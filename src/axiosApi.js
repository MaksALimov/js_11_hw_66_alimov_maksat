import axios from 'axios';

const axiosApi = axios.create({
    baseURL: 'https://my-blog-lab-64-default-rtdb.firebaseio.com/',
});

export default axiosApi;