import React, {useMemo, useState} from 'react';
import Spinner from "../components/Spinner/Spinner";

const withLoader = (WrappedComponent, axios) => {
    return props => {
        const [loading, setLoading] = useState(false);
        const [error, setError] = useState(null);

        useMemo(() => {
            axios.interceptors.request.use(req => {
                setLoading(true);
                return req;
            }, error => {
                setLoading(false);
                setError(error);
                throw error;
            });

            axios.interceptors.response.use(res => {
                setLoading(false);
                return res;
            }, error => {
                setLoading(false);
                setError(error);
                throw error;
            });

        }, []);

        return (
            <>
                {loading ? <Spinner/> : null}
                {error ? <h1>Произошла ошибка: {error.message}</h1> : <WrappedComponent {...props}/>}
            </>
        )
    };
};

export default withLoader;