import React, {useEffect, useState} from "react";
import axiosApi from "../../axiosApi";
import withLoader from "../../hoc/withLoader";
import './Posts.css';

const Posts = () => {
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        const postData = async () => {
            const response = await axiosApi.get('posts.json');
            setPosts(Object.keys(response.data).map(key => {
                return response.data[key];
            }))
        };

        postData().catch(e => console.log(e));
    }, []);

    return (
        <div>
            {posts.map((post, index) => (
                <div key={index} className="Wrapper">
                    <div>
                        <h3 className="Wrapper__title">Title</h3>
                        <p>{post.addPost.title}</p>
                    </div>
                    <div>
                        <h3 className="Wrapper__title">Text</h3>
                        <p>{post.addPost.textArea}</p>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default withLoader(Posts, axiosApi);